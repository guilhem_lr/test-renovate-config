module.exports = {
    platform: "gitlab",
    endpoint: process.env.CI_API_V4_URL,
    cache: false,
    branchPrefix: "fix/renovate-",
    onboardingBranch: "fix/renovate-configure",
    autodiscover: true,
    onboarding: true,
    automerge: true
  };
  